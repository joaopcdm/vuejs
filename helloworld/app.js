vue = new Vue({
  el: "#app",
  data: {
    name: "Anônimo",
    nickname: "anonimo",
    description: "Você está em modo anônimo.",
    clicks: 0,
    x: 0,
    y: 0
  },
  methods: {
    updateName: function (event) {
      if (event.target.value.length > 0) {
        this.name = event.target.value
        this.nickname = (this.name).toLowerCase()
        this.description = this.nickname + " está utilizando Vue.js!"
        this.clicks = 0
      } else {
        this.name = "Anônimo"
        this.nickname = (this.name).toLowerCase()
        this.description = "Você está em modo anônimo."
        this.clicks = 0
      }
    },
    updateCoordinates: function (event) {
      this.x = event.clientX;
      this.y = event.clientY;
    },
    increase: function () {
      this.clicks ++;
    }
  }
});